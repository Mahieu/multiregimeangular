import { Injectable } from '@angular/core';
import { User } from './user';
import { HttpClient } from '@angular/common/http';
import {environment} from 'src/environments/environment'
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  API_URL = environment.apiUrl +"auth/";
  constructor(private http : HttpClient) { }
  register(user : User): Observable<User> {
    console.log(this.API_URL+'signup/');
    return this.http.post<User>(this.API_URL+'signup', user);
  }
  login(user:User)
  {
    return this.http.post(this.API_URL+'signin/',user);
  }
  public getClaim(name: string): any {
    if (localStorage.getItem('token') == null) {
      return null;
    }

    interface jwtObj {
        username: string,
        iat: number,
        exp: number
    }
    let decodedClaims = atob(localStorage.getItem('token').split('.')[1]);
    let jwt: jwtObj = JSON.parse(decodedClaims);
    return jwt[name];
  }
  public setToken(token:string)
  {
    if(token!= null)
    {
      localStorage.setItem("token",token);
    }
  }
  public removeToken()
  {
    localStorage.removeItem("token");
  }
  public getExp(): number {
    return this.getClaim("exp");
  }
  public getIat() : number{
    return this.getClaim("iat");
  }
  private isLoggedOut() {
    return !this.isLoggedIn();
  }
  public isLoggedIn() {
    return (Date.now() / 1000 < this.getClaim("exp"))
  }
}
