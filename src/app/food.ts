
export class Food {
    id: number;
    name:string;
    displayName: string;
    portionDisplayName: string;
    servingDesc: string;
    category : string;
    points : number;
    type : string;
    weight : string;
    weightType : string;
    brand : string;
}