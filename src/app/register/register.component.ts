import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PointsService } from '../points.service';
import { User } from '../user';
import { UserService } from '../user.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registrationForm = new FormGroup({
    username: new FormControl('', [
      Validators.required,
    ]),
    password: new FormControl('', [
      Validators.required,
    ]),
    height : new FormControl('', [
      Validators.required,
    ]),
    weight: new FormControl('', [
      Validators.required,
    ]),
    gender: new FormControl('', [
      Validators.required,
    ]),
    age : new FormControl('', [
      Validators.required,
    ]),
    points : new FormControl('', [
      Validators.required,
    ]),
  });
  constructor(private pointsService: PointsService,private userService : UserService) { }

  ngOnInit() {
  }
  onSubmit() {
    let user : User;
    user = this.registrationForm.value;
    user.gender = (this.registrationForm.get('gender').value == 'true')
    this.userService.register(user).subscribe(user => console.log(user));
  }

}
