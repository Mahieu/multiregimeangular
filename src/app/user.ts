import { Meal } from './meal';

export class User {
    id: number;
    username: string;
    password: string;
    meals: Meal[];
    height : number;
    weight:number;
    gender:boolean;
    points:number;
    age:number;
}
