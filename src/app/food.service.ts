import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Food } from './food';
import { Observable } from 'rxjs';
import { SearchDto } from './food/dto/search.dto';
import { UtilsService } from './utils.service';

@Injectable({
  providedIn: 'root'
})
export class FoodService {

  API_URL = environment.apiUrl +"foods/";
  constructor(private http : HttpClient) { }
  getFood(params?: SearchDto): Observable<Food[]> {
    const queryParams: HttpParams = new HttpParams().set('search',params.search);
    return this.http.get<Food[]>(this.API_URL,{params : queryParams});
  }
}
