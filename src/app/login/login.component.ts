import { Component, OnInit} from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { UserService } from '../user.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm = new FormGroup({
    username: new FormControl('', [
      Validators.required,
    ]),
    password: new FormControl('', [
      Validators.required,
    ])});
  constructor(private userService:UserService) { }

  ngOnInit() {
  }
  onSubmit() {
    let user = this.loginForm.value;
    this.userService.login(user).subscribe((res : {accessToken:string}) => this.userService.setToken(res.accessToken));
  }
}
