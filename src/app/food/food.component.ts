import { Component, OnInit, ViewChild } from '@angular/core';
import { Food } from '../food';
import { FoodService } from '../food.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { SearchDto } from './dto/search.dto';
import { HttpParams } from '@angular/common/http';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.css']
})
export class FoodComponent implements OnInit {
  foods : Food[];
  searchParams : SearchDto;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  dataSource = new MatTableDataSource<Food>();
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(private foodService:FoodService) { }
  displayedColumns: string[] = ['name','servingDesc','category','brand','points'];
  ngAfterViewInit(): void {   
    this.dataSource.paginator = this.paginator;
    this.paginator.page.subscribe((pageEvent) => this.pageEventHandler(pageEvent));
  }
  ngOnInit() {
  }
  pageEventHandler(event : PageEvent)
  {
    //this.searchParams = new SearchDto(null,String(event.pageSize),String(event.pageSize*event.pageIndex))
    //this.foodService.getFood(this.searchParams).subscribe(foods => this.dataSource.data = foods);
  }
  search(filter)
  {
    console.log(filter);
    this.searchParams = new SearchDto(filter,"","")
    this.foodService.getFood(this.searchParams).subscribe(foods => this.dataSource.data = foods);
  }
}
